---
name: SyDRA 
network: Inria-DGRSRT
role: coordinator
duration: 2006 &mdash; 2007
---

Bilateral project (Inria-DGRSRT program STIC Tunisie 2006) between the Inria team Secsi (Saclay), the LaBRI (U. Bordeaux) and the team Digital Security (Tunis, Sup’Com) on the theme Automated inductive theorem proving techniques for the validation of protocols and distributed systems.
