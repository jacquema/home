---
name: Rossignol 
link: "http://www.lsv.fr/Projects/anr-avote/"
network: ACI Securite Informatique
role: responsible for the partner LSV/ENS Cachan 
duration: 2003 &mdash; 2006
---

Action Concertee Incitative between team MOVE (LIF, Marseille), team SECSI (Inria, LSV/ENS Cachan) and VERIMAG (Grenoble) on Algebraic properties and probabilistic models of security protocols.
