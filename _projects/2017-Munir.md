---
name: Munir 
link: "http://munir.cnam.fr"
network: ANR-FRQSC-2
fullname: Music Notation Information Retrieval
duration: 2017 &mdash; 2020
---

Project between CEDRIC/CNAM and Mc Gill and Montréal U.  
I co-direct, with Philippe Rigaux at CNAM, the PhD of Francesco Foscarin on transcription, funded by this project.
