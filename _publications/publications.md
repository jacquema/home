---
title: publications
description: publication list
---

Selected publications in reverse chronological order 
(in our community, the authors are generally listed by alphabetic ordering).
 	
	
* [publication list](https://haltools.inria.fr/Public/afficheRequetePubli.php?auteur_exp=Florent+Jacquemard&annee_publideb=2007&CB_auteur=oui&CB_titre=oui&CB_article=oui&langue=Anglais&tri_exp=annee_publi&tri_exp2=typdoc&tri_exp3=date_publi&ordre_aff=TA&Fen=Aff&css=../css/VisuRubriqueEncadre.css) since 2007 from HAL (as provided by [HAL-INRIA](https://hal.inria.fr)).
* [Habilitation a diriger des recherches](https://tel.archives-ouvertes.fr/tel-00643595/file/plan.pdf) - 2011 ([slides]({{ site.url }}/files/slides/slides-hdr.pdf)).
