---
permalink: /teaching/
title: teaching
description: teaching activities
---

Some teaching activities:

* Lecture on _Tree Automata Theory_ at Master Parisien de Recherche en Informatique (MPRI) between 2006 and 2011 (36h/year).

* Teaching assistant for the lecture of Jean Goubault Larrecq on _Lambda Calculus_ at Master Parisien de Recherche en Informatique (MPRI), École Normale Supérieure de Paris, between 2003 and 2006.

* Co-author of the collective online book [_Tree Automata Techniques and Applications_](http://tata.gforge.inria.fr/), widely used for teaching this discipline in universities.

* lecture (3h) at the [International School on Rewriting](https://isr2019.inria.fr), 2019, on _Rewriting for Music_.

* Lecture (3h) for the opening semester seminar 2017 at École Normale Supérieure Paris-Saclay, on _Music and Automata_.

* Lecture (6h) at the summer school [VTSA 2011](https://resources.mpi-inf.mpg.de/departments/rg1/conferences/vtsa11/) (Verification Technology, Systems & Applications), Liège, Belgium, on _Tree Automata Techniques for Infinite Systems Verification_.