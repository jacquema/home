---
permalink: /research/
title: research
description: research interests
---

My research project is concerned with  Computer Music and Digital Humanities, 
more specifically with the processing of structured music data (*digital music scores*), 
and in particular the following problems:

[comment]: # *  the **creation** and **digitalisation** of music notation (assistance to composition and corpus captation), 
* music **transcription**
* music score and corpus **analysis** (digital musicology)
  and **melodic similarity** computation
* **information retrieval** in bases of music scores

using on formal methods and tools from the following fundamental domains:
* automata and **tree automata** theory, **weighted** and unweighted, 
* **edit distances** between strings and trees, 
* logic in computer science, 
* term rewriting systems and automated deduction.

Formerly, I have worked on these formal methods and their application to the verification of systems and software:
* timed-testing of interactive music systems and Computer-Aided Composition at **[Ircam](https://www.ircam.fr)**, Paris (team [Repmus](http://repmus.ircam.fr)),
* verification of web data management systems and computer security at **[LSV](http://www.lsv.fr)/ENS-Cachan**, 
* automated deduction at **[Inria Nancy](http://www.loria.fr)**, **[MPI-I](https://www.mpi-inf.mpg.de)** Saarbrücken, **[SRI International](http://www.csl.sri.com)**, Stanford,
* development of secure embedded software components for smartcards and payment terminals at the company **Trusted Logic**.

